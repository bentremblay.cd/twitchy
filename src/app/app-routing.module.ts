import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {JeuListeComponent} from "./jeu-liste/jeu-liste.component";
import {StreamerListeComponent} from "./streamer-liste/streamer-liste.component";
import {JeuDetailsComponent} from "./jeux/jeu-details/jeu-details.component";
import {JeuCreerComponent} from "./jeux/jeu-creer/jeu-creer.component";
import {JeuModifierComponent} from "./jeux/jeu-modifier/jeu-modifier.component";
import {AuthGuard} from "./auth/auth.guard";
import {ConnexionComponent} from "./auth/connexion/connexion.component";

const routes: Routes = [
  {
    path: '', component: JeuListeComponent
  },
  {
    path: 'connexion', component: ConnexionComponent
  },
  {
    path: 'jeux/nouveau', component: JeuCreerComponent, canActivate: [AuthGuard]
  },
  {
    path: 'jeux/:id', component: JeuDetailsComponent
  },
  {
    path: 'jeux/:id/modifier', component: JeuModifierComponent, canActivate: [AuthGuard]
  },
  {
    path: 'streamers', component: StreamerListeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
