import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamerListeComponent } from './streamer-liste.component';

describe('StreamerListeComponent', () => {
  let component: StreamerListeComponent;
  let fixture: ComponentFixture<StreamerListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreamerListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StreamerListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
