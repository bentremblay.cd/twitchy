import { Component, OnInit } from '@angular/core';
import {StreamersService} from "../core/services/streamers.service";

@Component({
  selector: 'app-streamer-liste',
  templateUrl: './streamer-liste.component.html',
  styleUrls: ['./streamer-liste.component.scss']
})
export class StreamerListeComponent implements OnInit {
  streamers: string[] = [];

  constructor(private streamersService: StreamersService) { }

  ngOnInit(): void {
    this.streamers = this.streamersService.obtenirStreamers();
  }

}
