import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FavorisService} from "../../core/services/favoris.service";
import {AuthService} from "../../core/services/auth.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  profil: any;

  /**
   * Il faut une référence au tableau pour qu'Angular détecte les changements au tableau et ajuste
   * le HTML selon les changements.
   * Une référence à une fonction count() du service, par exemple, ne fonctionnerait pas.
   */
  get nombreFavoris() {
    return this.favorisService.favoris.length;
  }

  constructor(private favorisService: FavorisService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  estConnecte() {
    return this.authService.estConnecte();
  }

  connexion() {
    this.router.navigate(['/connexion']);
  }

  deconnexion() {
    this.authService.deconnexion();
  }

}
