import { Component, OnInit } from '@angular/core';
import {JeuxService} from "../core/services/jeux.service";
import {Jeu} from "../core/models/jeu.model";

@Component({
  selector: 'app-jeu-liste',
  templateUrl: './jeu-liste.component.html',
  styleUrls: ['./jeu-liste.component.scss']
})
export class JeuListeComponent implements OnInit {
  jeux: Jeu[] = [];
  erreurMsg!: string;

  constructor(private jeuxService: JeuxService) { }

  ngOnInit(): void {
    this.jeuxService.obtenirJeux().subscribe({
      next: (jeux) => {
        this.jeux = jeux;
      },
      error: (erreur) => {
        this.erreurMsg = erreur.message;
      }
    });
  }

  retirerJeu(event: { jeu: Jeu, message: string }) {
    this.jeux.forEach( (jeu, index) => {
      if(jeu.id == event.jeu.id) {
        this.jeux.splice(index, 1);
        alert(event.message);
      }
    });
  }
}
