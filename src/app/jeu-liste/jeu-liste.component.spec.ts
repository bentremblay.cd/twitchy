import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JeuListeComponent } from './jeu-liste.component';

describe('JeuListeComponent', () => {
  let component: JeuListeComponent;
  let fixture: ComponentFixture<JeuListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JeuListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JeuListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
