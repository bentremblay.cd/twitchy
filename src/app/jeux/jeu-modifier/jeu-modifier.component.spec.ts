import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JeuModifierComponent } from './jeu-modifier.component';

describe('JeuModifierComponent', () => {
  let component: JeuModifierComponent;
  let fixture: ComponentFixture<JeuModifierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JeuModifierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JeuModifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
