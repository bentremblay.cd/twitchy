import { Component, OnInit } from '@angular/core';
import {Jeu} from "../../core/models/jeu.model";
import {FormBuilder, Validators} from "@angular/forms";
import {JeuxService} from "../../core/services/jeux.service";
import {ActivatedRoute, Router} from "@angular/router";


/**
 * Composante permettant de modifier un jeu via un formulaire
 */
@Component({
  selector: 'app-jeu-modifier',
  templateUrl: './jeu-modifier.component.html',
  styleUrls: ['./jeu-modifier.component.scss']
})
export class JeuModifierComponent implements OnInit {
  /**
   * Référence au jeu à modifier
   */
  jeu!: Jeu;

  /**
   * Formulaire de modification de jeu
   */
  formulaire = this.fb.group({
    id: [0],
    nom: ['', Validators.required],
    description: ['', [Validators.required, Validators.minLength(50)]],
  }, {
    updateOn: 'submit'
  });

  /**
   * Retourne la valeur du champ nom tel que dans le formulaire
   */
  get nom() {
    return this.formulaire.get('nom');
  }

  /**
   * Retourne la valeur du champ description tel que dans le formulaire
   */
  get description() {
    return this.formulaire.get('description');
  }

  /**
   * Crée la composante
   * @param jeuxService Référence au service des jeux
   * @param fb FormBuilder utilisé pour construire et le formulaire et contient les données entrées
   * @param route Route courante (activée)
   * @param router Routeur Angular
   */
  constructor(private jeuxService: JeuxService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.jeuxService.obtenirJeu(id).subscribe((jeu) => {
      this.jeu = jeu;
      this.formulaire.setValue({
        id: jeu.id,
        nom: jeu.nom,
        description: jeu.description
      })
    });
  }

  /**
   * Fonction appelée lorsque le formulaire est envoyé (bouton submit)
   */
  soumettre() {
    if(this.formulaire.valid) {
      this.jeuxService.modifierJeu(this.formulaire.value as Jeu).subscribe(() => {
        this.router.navigate(['jeux', this.jeu.id]);
      })
    }
  }
}
