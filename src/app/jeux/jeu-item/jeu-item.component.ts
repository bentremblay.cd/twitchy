import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Jeu} from "../../core/models/jeu.model";
import localeFr from "@angular/common/locales/fr";
import {registerLocaleData} from "@angular/common";
import {FavorisService} from "../../core/services/favoris.service";

@Component({
  selector: 'app-jeu-item',
  templateUrl: './jeu-item.component.html',
  styleUrls: ['./jeu-item.component.scss']
})
export class JeuItemComponent implements OnInit {
  @Input() jeu!: Jeu;
  @Output() retirerJeuEvenement = new EventEmitter<{ jeu: Jeu, message: string }>;
  visionnementDisponible: boolean = true;
  estFavori = false;

  constructor(private favorisService: FavorisService) { }

  ngOnInit(): void {
    this.estFavori = this.favorisService.estFavori(this.jeu);

    registerLocaleData(localeFr, 'fr');

    this.miseAjourSpectateurs();

    setInterval( () => {
      this.miseAjourSpectateurs();
    }, 5000);
  }

  retirer() {
    this.retirerJeuEvenement.emit({
      jeu: this.jeu,
      message: `${this.jeu.nom} retiré de la liste!`
    });
  }

  favori(event: Event) {
    this.estFavori ? this.favorisService.retirer(this.jeu) : this.favorisService.ajouter(this.jeu);
    this.estFavori = !this.estFavori;
    (event.target as HTMLButtonElement).blur();
  }

  private miseAjourSpectateurs(): void {
    this.jeu.spectateurs = Math.round(Math.random()*10000);
  }

}
