import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JeuCreerComponent } from './jeu-creer.component';

describe('JeuCreerComponent', () => {
  let component: JeuCreerComponent;
  let fixture: ComponentFixture<JeuCreerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JeuCreerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JeuCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
