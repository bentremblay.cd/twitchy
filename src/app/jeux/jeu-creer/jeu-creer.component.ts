import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {JeuxService} from "../../core/services/jeux.service";
import {Router} from "@angular/router";
import {Jeu} from "../../core/models/jeu.model";
import {jeuListeExclusionValidator} from "../../core/validators/jeu-liste-exclusion-validator.directive";

@Component({
  selector: 'app-jeu-creer',
  templateUrl: './jeu-creer.component.html',
  styleUrls: ['./jeu-creer.component.scss']
})
export class JeuCreerComponent implements OnInit {
  nouveauJeuFormulaire = this.fb.group({
    nom: ['', [Validators.required, jeuListeExclusionValidator()]],
    description: ['', [Validators.required, Validators.minLength(50)]]
  }, {
    updateOn: 'submit'
  })

  constructor(private jeuxService: JeuxService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  get nom() {
    return this.nouveauJeuFormulaire.get('nom');
  }

  get description() {
    return this.nouveauJeuFormulaire.get('description');
  }

  soumettre() {
    if(this.nouveauJeuFormulaire.valid) {
      let jeuFormulaire = this.nouveauJeuFormulaire.value as Jeu
      this.jeuxService.creerJeu(jeuFormulaire).subscribe( (jeu: Jeu) => {
        this.router.navigate(['']);
      })
    }
  }

}
