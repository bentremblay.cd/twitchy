import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {JeuxService} from "../../core/services/jeux.service";
import {Jeu} from "../../core/models/jeu.model";

@Component({
  selector: 'app-jeu-details',
  templateUrl: './jeu-details.component.html',
  styleUrls: ['./jeu-details.component.scss']
})
export class JeuDetailsComponent implements OnInit {
  jeu: Jeu | undefined;

  constructor(
    private route: ActivatedRoute,
    private jeuxService: JeuxService,
    private router: Router) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.jeuxService.obtenirJeu(id).subscribe((jeu) => {
      this.jeu = jeu;
    });
  }

  supprimerJeu(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.jeuxService.supprimerJeu(id).subscribe(() => {
      this.router.navigate(['']);
    })
  }

}
