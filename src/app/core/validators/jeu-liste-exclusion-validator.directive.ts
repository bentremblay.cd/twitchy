import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function jeuListeExclusionValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const exclusions = [
      'Hatred'
    ];

    return exclusions.includes(control.value) ? {jeuExclu: {value: control.value}} : null;
  }
}
