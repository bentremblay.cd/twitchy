/**
 * Modèle représentant un Jeu
 */
export interface Jeu {
  /**
   * Identifiant unique du Jeu
   */
  id: number;

  /**
   * Nom du jeu
   */
  nom: string;

  /**
   * Description du jeu
   */
  description: string;

  /**
   * Nombre de spectateurs visionnant des streams pour ce jeu
   */
  spectateurs: number;

  /**
   * Url de l'image associée au jeu
   */
  imageUrl?: string;
}
