import { Injectable } from '@angular/core';
import {Jeu} from "../models/jeu.model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class JeuxService {
  private apiUrl = `${environment.apiBaseUrl}/jeux`

  constructor(private httpClient: HttpClient) { }

  obtenirJeu(id: number): Observable<Jeu> {
    const url = `${this.apiUrl}/${id}`;
    return this.httpClient.get<Jeu>(url);
  }

  obtenirJeux(): Observable<Jeu[]> {
    return this.httpClient.get<Jeu[]>(this.apiUrl)
      .pipe(
        catchError(this.gestionErreur)
      )
  }

  creerJeu(jeu: Jeu): Observable<Jeu> {
    return this.httpClient.post<Jeu>(this.apiUrl, jeu);
  }

  modifierJeu(jeu: Jeu): Observable<any> {
    const url = `${this.apiUrl}/${jeu.id}`;
    return this.httpClient.put<Jeu>(url, jeu);
  }

  supprimerJeu(id: number): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.httpClient.delete(url);
  }

  private gestionErreur(erreur: HttpErrorResponse) {
    return throwError(()=>new Error('Une erreur est survenue'));
  }
}
