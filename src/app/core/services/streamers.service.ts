import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StreamersService {

  constructor() { }

  obtenirStreamers(): string[] {
    return [
      'Marc Arcand',
      'Claire Cliche',
      'Guylaine Guy',
      'Pierre Paul',
      'Pénéloppe Patenaude',
    ]
  }
}
