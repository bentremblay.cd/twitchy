import { Injectable } from '@angular/core';
import {Jeu} from "../models/jeu.model";

@Injectable({
  providedIn: 'root'
})
export class FavorisService {
  private _favoris: Jeu[] = [];

  get favoris() {
    return this._favoris;
  }

  constructor() {
    this._favoris = JSON.parse(localStorage.getItem('favoris') || '[]');
  }

  sauvegarder() {
    localStorage.setItem('favoris', JSON.stringify(this._favoris));
  }

  ajouter(favori: Jeu) {
    if(!this.estFavori(favori)) {
      this._favoris.push(favori);
      this.sauvegarder();
    }
  }

  retirer(favori: Jeu) {
    const index = this._favoris.findIndex(favoriStorage => favoriStorage.id === favori.id)
    this._favoris.splice(index, 1);
    this.sauvegarder();
  }

  estFavori(jeu: Jeu) {
    return this._favoris.some(favoriStorage => favoriStorage.id === jeu.id );
  }
}
