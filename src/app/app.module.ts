import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JeuListeComponent } from './jeu-liste/jeu-liste.component';
import { StreamerListeComponent } from './streamer-liste/streamer-liste.component';
import { JeuItemComponent } from './jeux/jeu-item/jeu-item.component';
import { JeuDetailsComponent } from './jeux/jeu-details/jeu-details.component';
import {HttpClientModule} from "@angular/common/http";
import { JeuCreerComponent } from './jeux/jeu-creer/jeu-creer.component';
import {ReactiveFormsModule} from "@angular/forms";
import { JeuModifierComponent } from './jeux/jeu-modifier/jeu-modifier.component';
import { ConnexionComponent } from './auth/connexion/connexion.component';
import { NavComponent } from './layout/nav/nav.component';
import {JwtModule} from "@auth0/angular-jwt";

@NgModule({
  declarations: [
    AppComponent,
    JeuListeComponent,
    StreamerListeComponent,
    JeuItemComponent,
    JeuDetailsComponent,
    JeuCreerComponent,
    JeuModifierComponent,
    ConnexionComponent,
    NavComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => localStorage.getItem('access_token'),
        allowedDomains: ['localhost:3000']
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
